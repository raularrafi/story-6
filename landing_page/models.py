from django.db import models
from datetime import datetime

class StatusDB(models.Model):
	status = models.TextField(max_length=300)
	date = models.DateTimeField(default=datetime.now, blank=True)

	def __str__(self):
		return self.date