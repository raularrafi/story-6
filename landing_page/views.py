from django.shortcuts import render, redirect
from landing_page.models import StatusDB
from landing_page.forms import StatusForm

def landing_page(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/landing-page/')
	else:
		form = StatusForm()

	objekStatus = StatusDB.objects.order_by('-date')
	return render(request, 'index.html', {'form': form, 'status': objekStatus})

def redirecting(request):
	return redirect('/landing-page/')