from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'landing_page'

urlpatterns = [
	path('landing-page/', views.landing_page, name='landing'),
	path('', views.redirecting, name='redirecting'),
]